# Development environment guide

[TOC]

## Description
This is a development environment guide for mac users. It is build upon the in build features of OS X and
take advantage of the packets manager [Homebrew](http://brew.sh/).

This repository also include setting files and programs to use.

*This repo has been inspired by [Chris Mallinson](https://mallinson.ca)
with this [article](https://mallinson.ca/osx-web-development/)*

## Setup the basics
In this part we will setup the basics of the environment. This includes stuff Xcode developer tools
and package managers like Homebrew.

### Xcode developer tools
First we need to install the Xcode command line developer tools. The following command will make an update
on your computer, you have to install it. This will include stuff like Git.

```
#!shell
xcode-select --install
```

### Homebrew - the packets manager for OS X
Then we should install [Homebrew](http://brew.sh/). To install it first open the terminal.
Copy and paste the following into the terminal:

```
#!shell
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

The installation will tell you what it does all the way.

**Optional - install apps through the terminal**

To install apps through the terminal we need to use [Homebrew Cask](http://caskroom.io/).
Just run the following command and you are ready to install apps!

```
#!shell
brew install caskroom/cask/brew-cask
```

You can [search](http://caskroom.io/search) for apps to install through cask.

To install an app use the following - %app_name% should be the app name:

```
#!shell
brew cask install %app_name%
```

As an example we can install Google Chrome like this.

```
#!shell
brew cask install google-chrome
```

You can install either Atom or Sublime Text for later use:

```
#!shell
brew cask install atom
```
```
#!shell
brew cask install sublime-text
```

## The server setup
In this part we will setup the server installations and configurations. This includes MySQL,
Apache (the build in version), PHP (the build in version) and dnsmasq as our DNS server.

### MySQL - database for almost anything
To install MySQL we will use the newly installed Homebrew. Run the following command:

```
#!shell
brew install mysql
```

To start, restart and stop the MySQL server use the following commands:

Start:
```
#!shell
mysql.server start
```

Restart:
```
#!shell
mysql.server restart
```

Stop:
```
#!shell
mysql.server stop
```

You can make the MySQL server start at launch with the following command:
```
#!shell
ln -sfv /usr/local/opt/mysql/*.plist ~/Library/LaunchAgents
```

**Security:**
It is recommended that you secure the server, run the following command:

```
#!shell
mysql_secure_installation
```

### dnsmasq - Our nice dns server
This will make us able to go to our testing sites through the .dev domain. We will redirect all .dev calls
to the localhost ip ```127.0.0.1```. This way we can have a nice workflow. This should be install and forget.

To install dnsmasq, use Homebrew:

```
#!shell
brew install dnsmasq
```

Now we need to setup the dns server to listen to .dev addresses. Copy and paste the following:

```
#!shell
cd $(brew --prefix)
mkdir etc
echo 'address=/.dev/127.0.0.1' > etc/dnsmasq.conf
sudo mkdir /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/dev'
```

Now to make it startup on login:

```
#!shell
sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
```

### Apache - The inbuild version, so no install
We will use the Apache server which is build into the mac from the beginning. There is nothing to install
here but, you can use the following commands to start, restart and stop the apache server:

Start:
```
#!shell
sudo apachectl start
```

Restart:
```
#!shell
sudo apachectl restart
```

Stop:
```
#!shell
sudo apachectl stop
```

#### Setup Apache for use with the build in PHP library
To use the build in PHP library we need to activate it through the ```httpd.conf``` file. This file is located
here ```/private/etc/apache2/httpd.conf```. If you installed Atom or Sublime Text you can open the file with
the following commands

```
#!shell
atom /private/etc/apache2/httpd.conf
```
```
#!shell
subl /private/etc/apache2/httpd.conf
```

Now on line ```169``` in Yosemite and El Capitan, uncomment the following line.

```#LoadModule php5_module libexec/apache2/libphp5.so```

To this:

```LoadModule php5_module libexec/apache2/libphp5.so```

#### Setup vhost in Apache
First we need to remove the directory protection from Apache. In the same file (```httpd.conf```)
at line ```219``` remove or comment the following lines:

```
#!apache
<Directory />
    AllowOverride none
    Require all denied
</Directory>
```

Now we need to activate the **mod_vhost_alias** module. Uncomment line ```160```

```#LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so```

So it becomes:

```LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so```

Now we have to include the ```httpd-vhosts.conf``` file. Go to line ```499``` and uncomment the following line:

```#Include /private/etc/apache2/extra/httpd-vhosts.conf```

So it becomes:

```Include /private/etc/apache2/extra/httpd-vhosts.conf```

Now it's time to edit the virtual hosts so Apache now knows which directory to use.
Open the ```httpd-vhosts.conf``` file in an editor, like Atom or Sublime:

```
#!shell
atom /private/etc/apache2/extra/httpd-vhosts.conf
```
```
#!shell
subl /private/etc/apache2/extra/httpd-vhosts.conf
```

You can comment everything or simply remove it from the file. We shall replace it with the following:

```
#!apache
<Directory "/www">
	Options Indexes MultiViews FollowSymLinks
	AllowOverride All
	Order allow,deny
	Allow from all
</Directory>

<Virtualhost *:80>
	VirtualDocumentRoot "/www/home"
	ServerName home.dev
	UseCanonicalName Off
</Virtualhost>

<Virtualhost *:80>
	VirtualDocumentRoot "/www/%2/%1"
	ServerName sites.dev
	ServerAlias *.dev
	UseCanonicalName Off
</Virtualhost>
```

This makes us able to acces our home page at *[home.dev](http://home.dev)* and each client page on *client-name.sites.dev*

Remember to restart the Apache server ```sudo apachectl restart```


#### Trouble making PHP write files to the server?
This might be a permission issue in the folder. An easy fix is to tell Apache to execute files as you.


Open ```/etc/apache2/httpd.conf``` and search for ```<IfModule unixd_module>```. In this section change user to
your user and group to 'staff' like below.

**Tip:** You can find your username by running ```whoami``` in the terminal

```
#!apache
#User _www
#Group _www

User username
Group staff
```

Remember to restart the Apache server ```sudo apachectl restart```





# Optional PHP configurations

## Multiple PHP versions

It is possible to install the exact version you want through Homebrew. You can even install
multiple versions and then switch them when needed. Just run ```brew install php{version}```.
If i want to install PHP 5.6 that would be ```brew install php56```.

### PHP Switcher

Having multiple versions of PHP can be a pain and hard to manage. sgotre has created a
(PHP switcher)[https://github.com/sgotre/sphp-osx] script for this perpuse. The repository will
be inside this as a submodule for easier access.

To make the switcher script work, remove all LoadModule from httpd.conf for PHP and insert this:

```
#!apache
# Brew PHP LoadModule for `sphp` switcher
LoadModule php5_module /usr/local/lib/libphp5.so
#LoadModule php7_module /usr/local/lib/libphp7.so
```

Then run the script and everything should work!

### PHP 7.0

To use PHP 7.0 you have to make some additional changes in the Apache configuration.

In the httpd.conf after this:

```
#!apache
<FilesMatch "^\.([Hh][Tt]|[Dd][Ss]_[Ss])">
    Require all denied
</FilesMatch>
```

Insert:

```
#!apache
<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>
```

You have to manually set the directory indexes for PHP too. Look for:

```
#!apache
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>
```

Edit to:

```
#!apache
<IfModule dir_module>
    DirectoryIndex index.php index.html
</IfModule>
```

### Xdebug

To add xdebug, simply run ```brew install php{version}-xdebug```. For PHP 5.6 that would be
```brew install php56-xdebug```.